# BI-PJS.1

## Prosemináře

1. [Základní syntaxe a datové typy](01-basics.md)
1. [Vývojové prostředí, debugování, pracovní workflow](02-debug.md)
1. [Operátory, výrazy a řídicí struktury](03-operators-expressions.md)
1. [Funkce](04-functions.md)
1. [Built-in funkce/objekty (řětězce, reg výrazy, datové typy)](05-builtin.md)
1. [Výjimky a chyby](06-errors.md)
1. [Základy OOP (prototyp, viditelnost)](07-objects.md)
1. [Používání OOP - closure, „dědičnost“, porovnávání objektů, serializace objektů](08-using-objects.md)
1. [Události](09-events.md)
1. [Javascript a web - Document Object Model](10-dom.md)
1. [Závislosti projektu - npm, webpack, Gulp, Grunt](11-dependency.md)
1. [ECMAScript 6 nové vlastnosti a možnosti](12-ecmascript6.md)

## Cvičení

1. [Základy Javascriptu](tutorials/01-basics.md)
1. Vývojové prostředí, debugování, pracovní workflow
1. [Operátory, výrazy a řídicí struktury](tutorials/03-operators-expressions.md)
1. [Funkce](tutorials/04-functions.md)
1. [Built-in funkce, regulární výrazy](tutorials/05-builtin.md)
1. [Základy OOP](tutorials/06-objects.md)
1. [Použití OOP](tutorials/07-using-objects.md)
1. [Události](tutorials/08-events.md)
1. [Závislosti projektu](tutorials/09-dependency.md)
1. [Document Object Model](tutorials/10-dom.md)
1. Rezerva/Konzultace
