# Události - cvičení

> [Teoretická část](../09-events.md)

- Upravte kód pro použití ve webovém prohlížeči (`require`, `module.exports`, ...). Můžete použít HTML šablonu.

```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Úkolníček | BI-PJS.1</title>
	<script src="...models.js" type="text/javascript"></script>
	<script src="...app.js" type="text/javascript"></script>
</head>
<body>
	<h1>Úkolníček</h1>
	<div id="content"></div>
</body>
</html>
```

- Vytvořte metodu pro uživatelsky přívětivý výpis úkolů zadaného uživatele (příp. upravte stávající) na webové stránce
- Vytvořte obsluhu události odškrtnutí/zaškrtnutí checkboxu pro splnění úkolu. Při události se popis úkolu přeškrtne, pokud je úkol splněn. Stav úkolu se změní také v datovém modelu.
