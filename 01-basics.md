# Základy Javascriptu

## Vlastnosti Javascriptu

- skriptovací jazyk
- zejména na klientské straně (v prohlížeči) nyní i na serveru - node.js
- interpretovaný
- objektově orientovaný
- vychází z jazyka [ECMAScript](https://cs.wikipedia.org/wiki/ECMAScript#Verze)

## Syntaxe

- příkazy oddělené `;`
- bloky příkazů v `{` a `}`
- proměnné `var x;`
- funkce uvozené `function f(x, y) { }`

## Proměnné

- case-sensitive
- první znak nesmí být číslo - `[_$A-Za-z][_$A-Za-z0-9]*`
- povolené znaky v názvech proměnných jsou i většina ISO 8859-1 a Unicode viz [blog](https://mathiasbynens.be/notes/javascript-identifiers-es6)

## Deklarace proměnných

- `var` - deklaruje proměnnou a volitelně nastavuje hodnotu
- `let` - deklaruje proměnnou v kontextu bloku a volitelně nastavuje hodnotu
- `const` - deklaruje konstantu a nastavuje hodnotu

## Deklarace - `var` vs `let`

```javascript
var x = 1;
{ var x = 2; } // x není v kontextu bloku
console.log(x); // 2
```

```javascript
if (true) {
  var x = 5;
}
console.log(x);  // x is 5
```

Deklarace `let` - lokální kontext

```javascript
if (true) {
  let y = 5;
}
console.log(y);  // ReferenceError: y is not defined
```

## Vyhodnocování proměnných

- výchozí hodnota `undefined`

```javascript
var x;

if(x === undefined){
  // ...
} else {
  // ...
}

x = x + 2; // NaN = Not a number

var y = null;
y = y + 2; // 2
```

## Variable hoisting (a)

- hoisting (vyzdvihnutí)
- odkaz na proměnnou deklarovanou později nezpůsobí výjimku

```javascript
console.log(x === undefined); // true
var x = 3;
```
se vyhodnotí takto

```javascript
var x;
console.log(x === undefined); // true
x = 3;
```

## Variable hoisting (b)

```javascript
console.log(z === undefined); // test na nedefinovanou hodnotu, nikoliv celou proměnnou
var x = 3;
```
způsobí výjimku `ReferenceError: z is not defined`

## Variable hoisting (c)

```javascript
var myvar = "my value";
(function() {
  console.log(myvar); // undefined
  var myvar = "local value";
})();
```
se vyhodnotí takto (kvůli překrytí proměnné `myvar`)

```javascript
var myvar = "my value";
(function() {
  var myvar;
  console.log(myvar); // undefined
  myvar = "local value";
})();
```

## Konstanty

- nelze měnit ani předeklarovat

```javascript
const e = 2.71828;
e = 3.14159;     // TypeError: Assignment to constant variable.
function e() {}; // SyntaxError: Identifier 'e' has already been declared
var e;           // SyntaxError: Identifier 'e' has already been declared
```
- lze měnit atributy!

```javascript
const o = {'key': 'value'};
o.key = 'changed';
```

## Datové typy (a)

- primitivní
  - boolean - `true`, `false`
  - `null`
  - `undefined`
  - number
  - string
  - symbol (od ECMAScript 6)
- `Object`

## Datové typy (b)

- objektové ekvivalenty obalující primitivní typ
  - `String`
  - `Number`
  - `Boolean`
  - `Symbol`
- metoda vracející obalený primitivní typ `valueOf()`

## Datové typy (c)

- dynamicky typovaný jazyk
- automatická konverze

```javascript
var x = 10;
x = 'deset';
```

ve výrazu s číslem, řetězcem a operátorem `+` se číslo konvertuje na řetězec!

```javascript
"5" - 2 // 3
"5" + 3 // "53"
````

## Datové typy (d)

- explicitní konverze na číslo
  - `parseInt()`
  - `parseFloat()`

## Literály (řetězce)

```javascript
'hokus'
"pokus"
"O'Reilly"
`víceřádkový
řetězec`
// String interpolation
var x = 5;
`Result is ${x}` // Result is 5
function ahoj(x) { return 'ahoj ' + x; }
ahoj`svete` //  ahoj('svete') nebo ahoj(`svete`) -> 'ahoj svete'
```

## Literály (regulární výraz)

```javascript
var re = /[A-Z]-?[0-9]+/;
'A-A'.match(re)    // null
'A5'.match(re)     // [ 'A5', index: 0, input: 'A5' ]
'A-51'.match(re)   // [ 'A-51', index: 0, input: 'A-51' ]
```

## Literály (celá čísla)

- základ 10 (dekadická) - `0`, `111`, `119`, `-333`
- základ 8 (oktálová) - `016`, `0001`, `-0o71`
- základ 16 (hexadecimální) - `0x123`, `0x0101`, `-0xDEAD`
- základ 2 (binární) - `0b1`, `0b0101`, `-0b10`

## Literály (desetinná čísla)

- `2.718282`
- `-.123456789`
- `-3.2E+11`
- `.1e-24`

## Literály (pravdivostní hodnota)

- `true`
- `false`

## Literály (pole)

- objekt `Array`
- `["Praha", "Brno", "Bratislava"]`
- čárky navíc vytváří `undefined` položky
  - `var cities = ["Praha", , "Bratislava"]; // cities[1] == undefined`
- poslední čárka se ignoruje
  - `var cities = ["Praha", "Brno", , ]; // cities.length == 3`

## Literály (objekty)

```javascript
var address = {
    street: 'Křivá',
    number: { house: 25, flat: 'B' }
};
address.street             // 'Křivá'
address.number.flat        // 'B'
address.number['flat']     // 'B'
address['number'].flat     // 'B'
address['number']['flat']  // 'B'
```

## Literály (objekty)

```javascript
var x = 5;
var obj = {
    // zkratka za 'x: x'
    x,
    // metoda
    toString() {
     // volání rodiče
     return "d " + super.toString();
    },
    // dynamický název atributu 'attr42: 42'
    [ 'attr' + (40 + 2) ]: 42
};
```

## Komentáře

```javascript
// Jednořádkový komentář
var x = 1;
/*
 víceřádkový
 komentář
 */
x = x + 2;
```

## Zdroje

- [MDN Grammar and types](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_types)
- [Valid JavaScript variable names in ECMAScript 6](https://mathiasbynens.be/notes/javascript-identifiers-es6)
